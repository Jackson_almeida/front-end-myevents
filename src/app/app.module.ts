import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SigninPageComponent } from './pages/signin-page/signin-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { EventRegistrationSubpageComponent } from './pages/main-page/internal-pages/event-registration-subpage/event-registration-subpage.component';
import { EventListSubpageComponent } from './pages/main-page/internal-pages/event-list-subpage/event-list-subpage.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
@NgModule({
  declarations: [
    AppComponent,
    SigninPageComponent,
    RegisterPageComponent,
    EventRegistrationSubpageComponent,
    EventListSubpageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    CoreModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
