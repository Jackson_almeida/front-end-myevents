import { AuthComponent } from './auth.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigninPageComponent } from '../signin-page/signin-page.component'
import { RegisterPageComponent } from '../register-page/register-page.component'

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: SigninPageComponent
      },
      {
        path: 'register',
        component: RegisterPageComponent
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
