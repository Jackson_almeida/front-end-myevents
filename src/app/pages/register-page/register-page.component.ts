import { Component, OnInit } from '@angular/core';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
import { ToastService } from '../../toast.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  constructor(
    private service: RegisterService,
    private router: Router,
    private toastService: ToastService,
  ) { }

  username : string = '';
  email : string = '';
  password : string = '';
  confirmPassword : string = '';

  getUsername(name : KeyboardEvent) : void {
    this.username = (<HTMLInputElement>name.target).value;
  }

  getEmail(email : KeyboardEvent) : void {
    this.email = (<HTMLInputElement>email.target).value;
  }

  getPassword(password : KeyboardEvent) : void {
    this.password = (<HTMLInputElement>password.target).value;
  }

  getConfirmPassword(confirmPassword : KeyboardEvent) : void {
    this.confirmPassword = (<HTMLInputElement>confirmPassword.target).value;
  }

  handleSendNewRegister() :void {
    if (!!this.username && !!this.email && this.password === this.confirmPassword) {
      const userObject : any = {
        username : this.username,
        email: this.email,
        password: this.password,
      };
      this.service.create(userObject).subscribe(
        success => {
          this.toastService.show('Cadastrado realizado com sucesso!', { classname: 'bg-success text-light', delay: 3000 });
          this.router.navigate(['login'])
        },
        error => error,
      )

    } else {
      console.error('ATENÇÃO: Registro Incorreto!')
    }
  }

  ngOnInit(): void {
  }

}
