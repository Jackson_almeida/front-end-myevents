import { EventEditService } from './event-edit.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventListSubpageService } from './../event-list-subpage/event-list.service';
import { ToastService } from '../../../../toast.service';

@Component({
  selector: 'app-event-edit-subpage',
  templateUrl: './event-edit-subpage.component.html',
  styleUrls: ['./event-edit-subpage.component.css']
})
export class EventEditSubpageComponent {

  constructor(
    private service : EventEditService,
    private serviceList : EventListSubpageService,
    private router : Router,
    private toastService: ToastService,
  ) {
    const event = this.router.getCurrentNavigation()?.extras?.state?.event;

    if (event) {
      this.eventId = event.id,
      this.eventName = event.eventname,
      this.description = event.description,
      this.initialDatetime = event.initialDatetime,
      this.finalDatetime = event.finalDatetime
    }
  }

  eventId : number = 0;
  eventName  : string = '';
  description : string = '';
  initialDatetime : string = '';
  finalDatetime : string = '';

  handleChangeEventName(event : any) {
    this.eventName = (<HTMLInputElement>event.target).value;
  }
  handleChangeEventDescription(event : any) {
    this.description = (<HTMLInputElement>event.target).value;
  }
  handleChangeEventInitialDatetime(event : any) {
    this.initialDatetime = (<HTMLInputElement>event.target).value;
  }
  handleChangeEventFinalDatetime(event : any) {
    this.finalDatetime = (<HTMLInputElement>event.target).value;
  }

  setEventEdit() {
    const eventObject = {
      eventname: this.eventName,
      description: this.description,
      initialDatetime: this.initialDatetime,
      finalDatetime: this.finalDatetime
    }
    this.service.eventEdit(this.eventId, eventObject).subscribe(
      success => {
        this.toastService.show('Evento editado com sucesso!', { classname: 'bg-success text-light', delay: 3000 });
        this.router.navigate(['listar']);
        this.serviceList.getEvents();
      },
      error => error
    )
  }

}
