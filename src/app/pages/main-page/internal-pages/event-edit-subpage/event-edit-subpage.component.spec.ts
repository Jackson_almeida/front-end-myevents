import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventEditSubpageComponent } from './event-edit-subpage.component';

describe('EventEditSubpageComponent', () => {
  let component: EventEditSubpageComponent;
  let fixture: ComponentFixture<EventEditSubpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventEditSubpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventEditSubpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
