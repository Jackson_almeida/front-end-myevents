import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventEditSubpageComponent } from './event-edit-subpage.component';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventEditService {

  private readonly API = `${environment.backend}/events`;

  constructor(private http: HttpClient) { }

  eventEdit(id: number, event : any) {
    const token = localStorage.getItem("Authorization")!;
    return this.http.put(`${this.API}/${id}`, event, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }

}
