import { Component, OnInit } from '@angular/core';
import { EventRegistrationService } from './event-registration.service';
import { Router } from '@angular/router';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbTime } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';
import { convertDate } from 'src/core/reusableFunctions/convertDate';
import { convertTime } from 'src/core/reusableFunctions/convertTime';
import { ToastService } from '../../../../toast.service';

@Component({
  selector: 'app-event-registration-subpage',
  templateUrl: './event-registration-subpage.component.html',
  styleUrls: ['./event-registration-subpage.component.css']
})
export class EventRegistrationSubpageComponent implements OnInit {

  constructor(
    private service : EventRegistrationService,
    private toastService: ToastService,
    private router: Router,
  ) { }

  eventName: string = '';
  eventDescription: string = '';
  initialDate: Date = new Date();
  finalDate: Date = new Date();
  initialTime: any;
  finalTime: any;


  onInitialDateChange(event: NgbDate) {
    this.initialDate.setDate(event.day);
    this.initialDate.setMonth(event.month);
    this.initialDate.setFullYear(event.year);
  }

  onFinalDateChange(event: NgbDate) {
    this.finalDate.setDate(event.day);
    this.finalDate.setMonth(event.month);
    this.finalDate.setFullYear(event.year);
  }

  onInitialTimeChange(event: NgbTime) {
    this.initialDate.setHours(event.hour);
    this.initialDate.setMinutes(event.minute);
    this.initialDate.setSeconds(event.second);
  }

  onFinalTimeChange(event: NgbTime) {
    this.initialDate.setHours(event.hour);
    this.initialDate.setMinutes(event.minute);
    this.initialDate.setSeconds(event.second);
  }

  setEvent(): any {
    if (!this.eventName || !this.eventDescription || !this.initialDate || !this.finalDate) {
      this.toastService.show('Preencha os campos!', { classname: 'bg-danger text-light', delay: 3000 });
      return;
    }

    this.initialDate.setHours(this.initialTime.hour);
    this.initialDate.setMinutes(this.initialTime.minute);
    this.initialDate.setSeconds(this.initialTime.second);
    this.finalDate.setHours(this.finalTime.hour);
    this.finalDate.setMinutes(this.finalTime.minute);
    this.finalDate.setSeconds(this.finalTime.second);

    if (this.initialDate < this.finalDate) {
      return this.service.create({
        'eventname': this.eventName,
        'description': this.eventDescription,
        'initialDatetime': this.initialDate,
        'finalDatetime': this.finalDate
      }).subscribe(
        () => {
          this.toastService.show('Evento criado com sucesso!', { classname: 'bg-success text-light', delay: 3000 });
          this.router.navigate(['listar']);
        },
        () => this.toastService.show('Algum erro ocorreu', { classname: 'bg-danger text-light', delay: 3000 }),
      );
    } else {
      this.toastService.show('Data inicial maior que final', { classname: 'bg-danger text-light', delay: 3000 });
    }
  }

  ngOnInit(): void {
  }

}
