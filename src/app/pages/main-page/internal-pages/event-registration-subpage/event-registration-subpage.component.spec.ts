import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventRegistrationSubpageComponent } from './event-registration-subpage.component';

describe('EventRegistrationSubpageComponent', () => {
  let component: EventRegistrationSubpageComponent;
  let fixture: ComponentFixture<EventRegistrationSubpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventRegistrationSubpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventRegistrationSubpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
