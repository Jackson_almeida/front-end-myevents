import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventRegistrationService {

  private readonly API = `${environment.backend}/events`;

  constructor(private http: HttpClient) { }

  create(events: any) {
    const token = localStorage.getItem("Authorization")!;
    return this.http.post(this.API, events, {
      headers: {
        'Authorization': `Bearer ${token}`,
      },
    });
  }

}
