import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventListSubpageComponent } from './event-list-subpage.component';

describe('EventListSubpageComponent', () => {
  let component: EventListSubpageComponent;
  let fixture: ComponentFixture<EventListSubpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventListSubpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventListSubpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
