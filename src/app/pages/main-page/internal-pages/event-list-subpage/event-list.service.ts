import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventListSubpageService {

  private readonly API = `${environment.backend}/events`;

  token : any = ''

  constructor(private http: HttpClient) { }

  getEvents() {
    const token = localStorage.getItem("Authorization")!;
    return this.http.get(this.API, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }

  checkEmail(email: string) {
    return this.http.get(`${this.API}/${email}`)
  }

  deleteEventId(eventId : number) {
    const token = localStorage.getItem("Authorization")!;
    return this.http.delete(`${this.API}/${eventId}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }

}
