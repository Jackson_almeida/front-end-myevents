import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventListSubpageService } from './event-list.service';
import { EventEditService } from './../event-edit-subpage/event-edit.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '../../../../toast.service';

@Component({
  selector: 'app-event-list-subpage',
  templateUrl: './event-list-subpage.component.html',
  styleUrls: ['./event-list-subpage.component.css']
})
export class EventListSubpageComponent implements OnInit {

  arrayEvents: any = []

  currentDescription: string;

  email: string;

  constructor(
    private service : EventListSubpageService,
    private router : Router,
    // private edit : EventEditService,
    private modalService: NgbModal,
    private toastService: ToastService,
  ) { }

  deleteEvent(eventId : number) : any {
    this.service.deleteEventId(eventId).subscribe(
      success => {
        this.toastService.show('Evento removido com sucesso!', { classname: 'bg-success text-light', delay: 3000 });
        this.getEvents();
      }
    )
  }

  editEvent(event: any): void {
    this.router.navigate(['editar'], { state: { event } })
  }

  getEvents(): any {
    this.service.getEvents().subscribe(
      success => {
        this.arrayEvents = success;
      },
      error => error
    )
  }

  ngOnInit(): void {
    this.getEvents();
  }

  openModal(content: any, item: any) {
    this.currentDescription = item.description;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result
  }

}
