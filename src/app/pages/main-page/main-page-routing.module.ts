import { EventEditSubpageComponent } from './internal-pages/event-edit-subpage/event-edit-subpage.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPageComponent } from './main-page.component';
import { EventListSubpageComponent } from './internal-pages/event-list-subpage/event-list-subpage.component';
import { EventRegistrationSubpageComponent } from './internal-pages/event-registration-subpage/event-registration-subpage.component';
import { AuthGuard } from '../../core/auth.guard';


const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'listar',
        component: EventListSubpageComponent
      },
      {
        path: 'criar',
        component: EventRegistrationSubpageComponent
      },
      {
        path: 'editar',
        component: EventEditSubpageComponent
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
