import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-page-routing.module';
import { MainPageComponent } from './main-page.component';
import { EventEditSubpageComponent } from './internal-pages/event-edit-subpage/event-edit-subpage.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MainPageComponent, EventEditSubpageComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
  ]
})
export class MainPageModule { }
