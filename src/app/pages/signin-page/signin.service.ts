import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { User } from './signin.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SigninService {

  private readonly API = `${environment.backend}/users/login`;

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<User[]>(this.API);
  }

  create(user : any) {
    return this.http.post(this.API, user);
  }

}
