import { Component, OnInit } from '@angular/core';
import { SigninService } from './signin.service'
import { Router } from '@angular/router';
import { ToastService } from '../../toast.service';

@Component({
  selector: 'app-signin-page',
  templateUrl: './signin-page.component.html',
  styleUrls: ['./signin-page.component.css']
})
export class SigninPageComponent implements OnInit {

  constructor(
    private service: SigninService,
    private router: Router,
    private toastService: ToastService,
  ) { }

  email : string = '';
  password : string = '';

  getEmail(email : KeyboardEvent) : void {
    this.email = (<HTMLInputElement>email.target).value;
  }

  getPassword(password : KeyboardEvent) : void {
    this.password = (<HTMLInputElement>password.target).value;
  }

  handleSigninEmailPassword() : void {
    const userObject : any = {
      username : this.email,
      password : this.password
    };
    this.service.create(userObject).subscribe(
      success => {
        localStorage.clear();
        localStorage.setItem('Authorization', Object.values(success)[0]);
        this.router.navigate(['listar']);
      },
      () => this.toastService.show('Credenciais inválidas!', { classname: 'bg-danger text-light', delay: 3000 }),
    );
  }

  ngOnInit(): void {
  }

}
