export const convertTime = (time: any): string => {
  if (time.hour < 10) time.hour = '0' + time.hour;
  if (time.minute < 10) time.minute = '0' + time.minute;
  const formatedTime = `${time.hour}:${time.minute}:00`
  return formatedTime;
};
