export const convertDate = (date: any): string => {
  let formatedDate = date.getFullYear();
  formatedDate += "-"

  let month = date.getMonth();

  if(month < 10) month = "0" + month;

  formatedDate += month;
  formatedDate += "-";

  let day = date.getDate();

  if(day < 10) day = "0" + day;
  formatedDate += day;

  return formatedDate;
}
